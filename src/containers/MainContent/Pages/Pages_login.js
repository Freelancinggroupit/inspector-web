import React , {Component } from 'react';
import AUX from '../../../hoc/Aux_';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/action';
import axios from 'axios';
import Pages_login_2 from './Pages_login_2'
class Pages_login extends Component{
 
    componentDidMount() {
        if(this.props.loginpage === false)
        {
          this.props.UpdateLogin();
        }
        window.onpopstate  = (e) => {
          this.props.UpdateLoginAgain();
        }
       }
       constructor(props) {

        super(props);
        
        this.state={
        
        email: '',
        
        password:'',
        
        errors: {}
        
        }
        

this.handleChangeEmail = this.handleChangeEmail.bind(this);

this.handleChangePassword = this.handleChangePassword.bind(this);

this.submituserLoginForm = this.submituserLoginForm.bind(this);

}

handleChangeEmail(e) {

this.setState({email:e.target.value});

}

handleChangePassword(e) {

this.setState({password:e.target.value});

}

submituserLoginForm(e) {

e.preventDefault();

if (this.validateForm()) {

console.log(this.state);

var apiBaseUrl = "http://localhost:8080/user/login";

var data={

"user_email":this.state.email,

"password":this.state.password

}

var headers = {

'Content-Type': 'application/json',

}

console.log(data);

axios.post(apiBaseUrl, data, {headers: headers}).then(function (response) {

console.log(response);

if(response.data.success){

localStorage.setItem("u_code", encodeURIComponent(JSON.stringify(response.data.data)));

localStorage.setItem('is_done', true);

window.location.href = "/";

console.log("Login successfull");

}else{

alert(response.data.message);

}

}).catch(function (error) {

console.log(error);

});

}

}

validateForm() {

let errors = {};

let formIsValid = true;

if (!this.state.email) {

formIsValid = false;

errors["email"] = "*Please enter your email-ID.";

}

if (typeof this.state.email !== "undefined") {

//regular expression for email validation

var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

if (!pattern.test(this.state.email)) {

formIsValid = false;

errors["email"] = "*Please enter valid email-ID.";

}

}

if (!this.state.password) {

formIsValid = false;

errors["password"] = "*Please enter your password.";

}

if (typeof this.state.password !== "undefined") {

if (!this.state.password.match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {

formIsValid = false;

errors["password"] = "*Please enter secure and strong password.";

}

}

this.setState({

errors: errors

});

return formIsValid;

}
render(){
    return(
           <AUX>
		   <div className="accountbg"></div>
           <div className="wrapper-page">

            <div className="card">
                <div className="card-body">

                    <h3 className="text-center m-0">
                    {/* <img src="assets/images/logo.png" height="30" alt="logo" /> */}
                        <Link to="/" onClick={()=> this.props.UpdateLoginAgain()} className="logo logo-admin"><h4>Logo</h4></Link>
                    </h3>

                    <div className="p-3">
                        <h4 className="font-18 m-b-5 text-center">Welcome Back!</h4>
                        <p className="text-muted text-center">Sign in to continue to Inspector.</p>

                        <form className="form-horizontal m-t-30" method="post" name="userRegistrationForm" onSubmit= {this.submituserLoginForm}>
                        {/* <FormGroup> */}
                            <div className="form-group">
                                <label for="username">Email <span className="errorMsg">*</span></label>
                                <input type="email" className="form-control" id="username" placeholder="Enter Email" value={this.state.email} onChange={this.handleChangeEmail} />
                                <div className="errorMsg">{this.state.errors.email}</div>
                            </div>
                            {/* </FormGroup> */}
                            <div className="form-group">
                                <label for="userpassword">Password <span className="errorMsg">*</span></label>
                                <input type="password" className="form-control" id="userpassword" value={this.state.password} onChange={this.handleChangePassword} placeholder="Enter password" />
                                <div className="errorMsg">{this.state.errors.password}</div>

                            </div>

                            <div className="form-group row m-t-20">
                                <div className="col-sm-6">
                                    {/* <div className="custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" id="customControlInline" />
                                        <label className="custom-control-label" for="customControlInline">Remember me</label>
                                    </div> */}
                                </div>
                                <div className="col-sm-6 text-right">
                                    <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            <div className="form-group m-t-10 mb-0 row">
                                <div className="col-lg-6 m-t-20">
                                    <Link to="pages_recoverpw" className="text-muted"><i className="mdi mdi-lock"></i> Forgot your password?</Link>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div className="m-t-40 text-center">
                <p className="text-white">Don't have an account ? <Link to="pages_register" className="font-500 font-14 text-white font-secondary"> Signup Now </Link> </p>
                <p className="text-white">© {new Date().getFullYear()-1} -  {new Date().getFullYear()} ConnectIcons. Crafted with <i className="mdi mdi-heart text-danger"></i> by Our Teams</p>
            </div>
        </div>
           </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        loginpage: state.ui_red.loginpage
    };
}

const mapDispatchtoProps = dispatch => {
    return {
        UpdateLogin: () => dispatch({ type: actionTypes.LOGINPAGE, value: true }),
        UpdateLoginAgain: () => dispatch({ type: actionTypes.LOGINPAGE, value: false })
    };
}

export default connect(mapStatetoProps, mapDispatchtoProps)(Pages_login);
