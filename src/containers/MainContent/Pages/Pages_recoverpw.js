import React , {Component } from 'react';
import AUX from '../../../hoc/Aux_';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/action';
import axios from 'axios';

class Pages_recoverpw extends Component{
    
    componentDidMount() {
        if(this.props.loginpage === false)
        {
          this.props.UpdateLogin();
        }
        window.onpopstate  = (e) => {
          this.props.UpdateLoginAgain();
        }
       }
       constructor(props) {

        super(props);
        
        this.state={
        
        email: '',
        
        password:'',

        mobileNumber:'',
        
        UserName :'',

        errors: {}
        
        }
        

this.handleChangeEmailForgot = this.handleChangeEmailForgot.bind(this);

this.submituserForgotPassword = this.submituserForgotPassword.bind(this);

}

handleChangeEmailForgot(e) {

this.setState({email:e.target.value});

}

submituserForgotPassword(e) {

e.preventDefault();

if (this.validateForm()) {

console.log(this.state);

var apiBaseUrl = "http://localhost:8080/user/forgetPassword?email=";

var headers = {

'Content-Type': 'application/json',

}

axios.post(apiBaseUrl+this.state.email, {headers: headers}).then(function (response) {

console.log(response);

if(response.data.success){

localStorage.setItem("u_code", encodeURIComponent(JSON.stringify(response.data.data)));

localStorage.setItem('is_done', true);

window.location.href = "/";

console.log("Login successfull");

}else{

alert(response.data.message);

}

}).catch(function (error) {

console.log(error);

});

}

}

validateForm() {

let errors = {};

let formIsValid = true;

if (!this.state.email) {

formIsValid = false;

errors["email"] = "*Please enter your email-ID.";

}

if (typeof this.state.email !== "undefined") {

//regular expression for email validation

var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

if (!pattern.test(this.state.email)) {

formIsValid = false;

errors["email"] = "*Please enter valid email-ID.";

}

}
this.setState({

errors: errors

});

return formIsValid;

}
render(){
    return(
           <AUX>
		   <div className="accountbg"></div>
          <div className="wrapper-page">

            <div className="card">
                <div className="card-body">

                    <h3 className="text-center m-0">
                        <Link to="/" onClick={()=> this.props.UpdateLoginAgain()}  className="logo logo-admin"><h4>Logo</h4></Link>
                    </h3>

                    <div className="p-3">
                        <h4 className="font-18 m-b-5 text-center">Reset Password</h4>
                        <p className="text-muted text-center">Enter your Email and instructions will be sent to you!</p>

                       <form className="form-horizontal m-t-30" name="userRegistrationForm" onSubmit= {this.submituserForgotPassword}>
                      
                            <div className="form-group">
                                <label for="useremail">Email</label>
                                <input type="email" className="form-control" id="useremail" placeholder="Enter email"  value={this.state.email} onChange={this.handleChangeEmailForgot} />
                                <div className="errorMsg">{this.state.errors.email}</div>
                            </div>
                            

                            <div className="form-group row m-t-20">
                                <div className="col-12 text-right">
                                    <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div className="m-t-40 text-center">
                <p className="text-white">Remember It ? <Link to="pages_login" className="font-500 font-14 text-white font-secondary"> Sign In Here </Link> </p>
                <p className="text-white">© {new Date().getFullYear()-1} -  {new Date().getFullYear()} ConnectIcons. Crafted with <i className="mdi mdi-heart text-danger"></i> by Our Teams</p>
            </div>
        </div>
           </AUX>
        );
    }
}


const mapStatetoProps = state => {
    return {
        loginpage: state.ui_red.loginpage
    };
}

const mapDispatchtoProps = dispatch => {
    return {
        UpdateLogin: () => dispatch({ type: actionTypes.LOGINPAGE, value: true }),
        UpdateLoginAgain: () => dispatch({ type: actionTypes.LOGINPAGE, value: false })
    };
}

export default connect(mapStatetoProps, mapDispatchtoProps)(Pages_recoverpw);