import React , {Component } from 'react';
import AUX from '../../../hoc/Aux_';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/action';
import axios from 'axios';

class Pages_register extends Component{

    componentDidMount() {
        if(this.props.loginpage === false)
        {
          this.props.UpdateLogin();
        }
        window.onpopstate  = (e) => {
          this.props.UpdateLoginAgain();
        }
       }
       constructor(props) {

        super(props);
        
        this.state={
        
        email: '',
        
        password:'',

        mobileNumber:'',
        
        UserName :'',

        errors: {}
        
        }
        

this.handleChangeEmail = this.handleChangeEmail.bind(this);

this.handleChangePassword = this.handleChangePassword.bind(this);

this.handleChangemobileNumber = this.handleChangemobileNumber.bind(this);

this.handleChangeUserName = this.handleChangeUserName.bind(this);

this.submituserRegistrationForm = this.submituserRegistrationForm.bind(this);

}

handleChangeEmail(e) {

this.setState({email:e.target.value});

}

handleChangePassword(e) {

this.setState({password:e.target.value});

}
handleChangemobileNumber(e){
    this.setState({mobileNumber:e.target.value});
}
handleChangeUserName(e){
    this.setState({UserName:e.target.value});
}
submituserRegistrationForm(e) {

e.preventDefault();

if (this.validateForm()) {

console.log(this.state);

var apiBaseUrl = "http://localhost:8080/user/account";

var data={
    "userId":null,
    "userName":this.state.UserName,
    "email":this.state.email,
    "mobileNumber":this.state.mobileNumber,
    "password": this.state.password,
    "status": true,
    "userAdditionalInfoDto":{
        "userAdditionalInfoId":null
    },
    "groupDto":{
        "groupId":1
    }
}

var headers = {

'Content-Type': 'application/json',

}

console.log(data);

axios.post(apiBaseUrl, data, {headers: headers}).then(function (response) {

console.log(response);

if(response.data.success){

localStorage.setItem("u_code", encodeURIComponent(JSON.stringify(response.data.data)));

localStorage.setItem('is_done', true);

window.location.href = "/";

console.log("Login successfull");

}else{

alert(response.data.message);

}

}).catch(function (error) {

console.log(error);

});

}

}

validateForm() {

let errors = {};

let formIsValid = true;

if (!this.state.email) {

formIsValid = false;

errors["email"] = "*Please enter your email-ID.";

}

if (typeof this.state.email !== "undefined") {

//regular expression for email validation

var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

if (!pattern.test(this.state.email)) {

formIsValid = false;

errors["email"] = "*Please enter valid email-ID.";

}

}

if (!this.state.password) {

formIsValid = false;

errors["password"] = "*Please enter your password.";

}
if (!this.state.mobileNumber) {

    formIsValid = false;
    
    errors["mobileNumber"] = "*Please enter your Mobile Number.";
    
    }

if (!this.state.UserName) {

        formIsValid = false;
        
        errors["UserName"] = "*Please enter your UserName.";
        
        }
if (typeof this.state.password !== "undefined") {

if (!this.state.password.match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {

formIsValid = false;

errors["password"] = "*Please enter secure and strong password.";

}

}

this.setState({

errors: errors

});

return formIsValid;

}
render(){
    return(
           <AUX>
		     <div className="accountbg"></div>
            <div className="wrapper-page">

            <div className="card">
                <div className="card-body">

                    <h3 className="text-center m-0">
                        <Link to="/" onClick={()=> this.props.UpdateLoginAgain()}  className="logo logo-admin"><h4>Logo</h4></Link>
                    </h3>

                    <div className="p-3">
                        <h4 className="font-18 m-b-5 text-center">Register</h4>
                        {/* <p className="text-muted text-center">Get your free Admiria account now.</p> */}

                        <form className="form-horizontal m-t-30" name="userRegistrationForm" onSubmit= {this.submituserRegistrationForm}>
                        <div className="form-group">
                                <label for="username">Username</label>
                                <input type="text" className="form-control" id="username" placeholder="Enter username" value={this.state.UserName} onChange={this.handleChangeUserName}  />
                                <div className="errorMsg">{this.state.errors.UserName}</div>
                            </div>
                            <div className="form-group">
                                <label for="useremail">Email</label>
                                <input type="email" className="form-control" id="useremail" placeholder="Enter email"  value={this.state.email} onChange={this.handleChangeEmail} />
                                <div className="errorMsg">{this.state.errors.email}</div>
                            </div>
                            <div className="form-group">
                                <label for="MobileNumber">Mobile Number</label>
                                <input type="text" className="form-control" id="MobileNumber" placeholder="Enter Mobile Number" value={this.state.mobileNumber} onChange={this.handleChangemobileNumber} />
                                <div className="errorMsg">{this.state.errors.mobileNumber}</div>
                            </div>
                        
                            <div className="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" className="form-control" id="userpassword" placeholder="Enter password" value={this.state.password} onChange={this.handleChangePassword} placeholder="Enter password" />
                                <div className="errorMsg">{this.state.errors.password}</div>
                            </div>

                            <div className="form-group row m-t-20">
                                <div className="col-12 text-right">
                                    <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Register</button>
                                </div>
                            </div>

                            <div className="form-group m-t-10 mb-0 row">
                                <div className="col-12 m-t-20">
                                    <p className="font-14 text-muted mb-0">By registering you agree to the Admiria <Link to="#">Terms of Use</Link></p>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div className="m-t-40 text-center">
                <p className="text-white">Already have an account ? <Link to="pages_login" className="font-500 font-14 text-white font-secondary"> Login </Link> </p>
                <p className="text-white">©  {new Date().getFullYear()-1} -  {new Date().getFullYear()} Connect Icons. Crafted with <i className="mdi mdi-heart text-danger"></i> by Our Teams</p>
            </div>

        </div>
		 
           </AUX>
        );
    }
}


const mapStatetoProps = state => {
    return {
        loginpage: state.ui_red.loginpage
    };
}

const mapDispatchtoProps = dispatch => {
    return {
        UpdateLogin: () => dispatch({ type: actionTypes.LOGINPAGE, value: true }),
        UpdateLoginAgain: () => dispatch({ type: actionTypes.LOGINPAGE, value: false })
    };
}

export default connect(mapStatetoProps, mapDispatchtoProps)(Pages_register);