import React , {Component } from 'react';
import AUX from '../../../hoc/Aux_';
import { Sparklines,SparklinesLine  } from 'react-sparklines';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from 'react-router-dom';
import Chart from 'react-apexcharts';
import MixedChart from '../../../containers/Chartstypes/Apex/MixedChart';
import DonutChart from '../../../containers/Chartstypes/Apex/DonutChart';
import CanvasJSReact from '../../../assets/canvas_js/canvasjs.react';
import "../../../App.css"
import $ from 'jquery'; 
// import { PieChart } from 'react-easy-chart';
// import { PieChart } from 'react-minimal-pie-chart';

// var React = require('react');
// var Component = React.Component;
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
class Dashboard extends Component{

    constructor(props) {
        super(props);
     
        this.state = {
          simple:80, simple68:68, simple37:37, simple72:72,
        };
      }
      componentDidMount(){
        // $('.canvasjs-chart-canvas span').text('Hi I am replace');
      }

      
 
render(){
    const options = {
        // theme: "dark2",
        animationEnabled: true,
        exportFileName: "New Year Resolutions",
        exportEnabled: true,
        title:{
            text: "Overall Completion"
        },
        data: [{
            type: "pie",
            showInLegend: true,
            legendText: "{label}",
            toolTipContent: "{label}: <strong>{y}%</strong>",
            indexLabel: "{y}%",
            indexLabelPlacement: "inside",
            dataPoints: [
                { y: 32, label: "Extraction" },
                { y: 22, label: "Mapping" },
                { y: 15, label: "Cleansing" },
                { y: 19, label: "QA" },
            ]
        }]
    }
    // second one start
    const options2 = {
        // theme: "dark2",
        animationEnabled: true,
        exportFileName: "New Year Resolutions",
        exportEnabled: true,
        title:{
            text: "KCP_12"
        },
        data: [{
            type: "pie",
            showInLegend: true,
            legendText: "{label}",
            toolTipContent: "{label}: <strong>{y}%</strong>",
            indexLabel: "{y}%",
            indexLabelPlacement: "inside",
            dataPoints: [
                { y: 32, label: "Extraction" },
                { y: 22, label: "Mapping" },
                { y: 15, label: "Cleansing" },
                { y: 19, label: "QA" },
            ]
        }]
    }

    // second one end
     // third one start
     const options3 = {
        // theme: "dark2",
        animationEnabled: true,
        exportFileName: "New Year Resolutions",
        exportEnabled: true,
        title:{
            text: "CBS_74"
        },
        data: [{
            type: "pie",
            showInLegend: true,
            legendText: "{label}",
            toolTipContent: "{label}: <strong>{y}%</strong>",
            indexLabel: "{y}%",
            // indexLabelPlacement: "inside",
            dataPoints: [
                { y: 32, label: "Extraction" },
                { y: 22, label: "Mapping" },
                { y: 15, label: "Cleansing" },
                { y: 19, label: "QA" },
            ]
        }]
    }

    // third one end
    return(
           <AUX>
			  <div className="page-content-wrapper">
              <div className="container-fluid">

                <div className="row">
                <div className="col-xl-4">
                <CanvasJSChart options = {options}/>
                </div>
                <div className="col-xl-4">
                {/* <PieCharts  size={70}
                                data={[
                                { key: 'A', value: 60, color: '#f2f2f2' },
                                { key: 'B', value: 200, color: '#1d1e3a' },
                                ]}
                            />  */}
                <CanvasJSChart options = {options2}/>
                </div>
                <div className="col-xl-4">
                {/* <PieChart
                data={[
                    { title: 'One', value: 10, color: '#E38627' },
                    { title: 'Two', value: 15, color: '#C13C37' },
                    { title: 'Three', value: 20, color: '#6A2135' },
                ]}
                /> */}
                <CanvasJSChart options = {options3}/>
                </div>
                
                </div><br/>

                <div className="row">
                    <div className="col-xl-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                {/* <h4 className="mt-0 m-b-30 header-title">Latest Transactions</h4> */}

                                <div className="table-responsive">
                                    <table className="table table-vertical mb-0">
                                    <tr>
                                    <th>Project Name</th>
                                    <th>Created By</th>
                                    <th>Reviewed By</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                    </tr>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img src="assets/images/users/avatar-2.jpg" alt="user-image" className="thumb-sm rounded-circle mr-2"/>
                                                Herbert C. Patton
                                            </td>
                                            <td><i className="mdi mdi-checkbox-blank-circle text-success"></i> Confirm</td>
                                            <td>
                                                $14,584
                                                <p className="m-0 text-muted font-14">Amount</p>
                                            </td>
                                            <td>
                                                5/12/2016
                                                <p className="m-0 text-muted font-14">Date</p>
                                            </td>
                                            <td>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; &nbsp;<i class="fa fa-eye" aria-hidden="true"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img src="assets/images/users/avatar-3.jpg" alt="user-image" className="thumb-sm rounded-circle mr-2"/>
                                                Mathias N. Klausen
                                            </td>
                                            <td><i className="mdi mdi-checkbox-blank-circle text-warning"></i> Waiting payment</td>
                                            <td>
                                                $8,541
                                                <p className="m-0 text-muted font-14">Amount</p>
                                            </td>
                                            <td>
                                                10/11/2016
                                                <p className="m-0 text-muted font-14">Date</p>
                                            </td>
                                            <td>
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; &nbsp;<i class="fa fa-eye" aria-hidden="true"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img src="assets/images/users/avatar-4.jpg" alt="user-image" className="thumb-sm rounded-circle mr-2"/>
                                                Nikolaj S. Henriksen
                                            </td>
                                            <td><i className="mdi mdi-checkbox-blank-circle text-success"></i> Confirm</td>
                                            <td>
                                                $954
                                                <p className="m-0 text-muted font-14">Amount</p>
                                            </td>
                                            <td>
                                                8/11/2016
                                                <p className="m-0 text-muted font-14">Date</p>
                                            </td>
                                            <td>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; &nbsp;<i class="fa fa-eye" aria-hidden="true"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img src="assets/images/users/avatar-5.jpg" alt="user-image" className="thumb-sm rounded-circle mr-2"/>
                                                Lasse C. Overgaard
                                            </td>
                                            <td><i className="mdi mdi-checkbox-blank-circle text-danger"></i> Payment expired</td>
                                            <td>
                                                $44,584
                                                <p className="m-0 text-muted font-14">Amount</p>
                                            </td>
                                            <td>
                                                7/11/2016
                                                <p className="m-0 text-muted font-14">Date</p>
                                            </td>
                                            <td>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; &nbsp;<i class="fa fa-eye" aria-hidden="true"></i>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img src="assets/images/users/avatar-6.jpg" alt="user-image" className="thumb-sm rounded-circle mr-2"/>
                                                Kasper S. Jessen
                                            </td>
                                            <td><i className="mdi mdi-checkbox-blank-circle text-success"></i> Confirm</td>
                                            <td>
                                                $8,844
                                                <p className="m-0 text-muted font-14">Amount</p>
                                            </td>
                                            <td>
                                                1/11/2016
                                                <p className="m-0 text-muted font-14">Date</p>
                                            </td>
                                            <td>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; &nbsp;<i class="fa fa-eye" aria-hidden="true"></i>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
           </AUX>
        );
    }
}

export default Dashboard;   